class Config(object):
	DEBUG = False
	TESTING = False
	SECRET_KEY = ''

	def __getattr__(self, name):
		if name == 'SECRET_KEY':
			if self.SECRET_KEY == '':
				print u'SECRET_KEY is not set, be careful'


class ProductionConfig(Config):
	DB_ADDRESS = ''
	
class DevConfig(Config):
	DEBUG = True
	# DB_ADDRESS = 'dev.db'
	SECRET_KEY = '$$testDeliC88ious`_kEy`'
	SERVER_NAME = 'localhost:8080'
	DB_NAME = 'tech_passports'
	DB_HOST = 'localhost'
	DB_PORT = 5432
	DB_USERNAME = 'pguser'
	DB_PASSWORD = 'pg_SecurE'