from flask import Flask, render_template, make_response, request, url_for, redirect, jsonify
import psycopg2
from sql import *
from sql.aggregate import *
from sql.conditionals import *

app = Flask(__name__)

app = Flask(__name__, static_folder = '../client/dist', static_url_path  = '/static')
app.config.from_object('app.settings.DevConfig')

def connect():
	conn = psycopg2.connect('dbname=%s user=%s host=%s password=%s' % \
			(app.config['DB_NAME'], \
			app.config['DB_USERNAME'], \
			app.config['DB_HOST'], \
			app.config['DB_PASSWORD'] ))
	return conn

fields = {
	'passport' : (
		'id', 
		'issued', 
		'created', 
		'type', 
		'location', 
		'area', 
		'area_sum', 
		'floors', 
		'floor_height',
		'size_description',
		'construction_date',
		'current_condition',
		'wall_material',
		'roof_material',
		'material_description',
		'infrastructure_description',
		'price')

	,'owner' : (
		'id',
		'name',
		'lastname',
		'patronymic',
		'phone_number',
		'address')

	,'contract' : (
		'id',
		'passport_id',
		'owner_id',
		'purchase_date',
		'price')
}

@app.route('/')
def index():
	try:
		conn = connect()
	except: 
		return make_response('Can\'t connect to database', 500)

	return render_template('index.html')

@app.route('/get/<table>/', methods=['GET'])
@app.route('/get/<table>/<id>', methods=['GET'])
def get(table = None, id = -1):
	resp = make_response()
	if table == None:
		resp.status = 400
		return resp
	try:
		conn = connect()
	except:
		return make_response('Can\'t connect to database', 500)

	model = Table(table)
	select = model.select()
	if not id == -1:
		select.where = model.id == str(id)
	query = tuple(select)

	cur = conn.cursor()
	try:
		cur.execute(query[0] % query[1])
		rows = cur.fetchall()
	except:
		return make_response('cant handle request', 500)
	conn.commit()
	conn.close()

	resp_rows = []
	for index, row in zip(range(len(rows)), rows):
		resp_rows.append({index : zip(fields[table], [str(field) for field in row])})
	# return make_response('OK', 200)
	return jsonify({'objects' : resp_rows})

@app.route('/add/<table>', methods=['POST'])
def add(table = None):
	resp = make_response()
	if table == None:
		resp.status = 400
		return resp
	try:
		conn = connect()
	except:
		return make_response('Can\'t connect to database', 500)

	model = Table(table)
	columns=[key for key in request.form if not len(request.form[key]) == 0 ]
	values=[request.form[key] for key in request.form if not len(request.form[key]) == 0]

	query = tuple(model.insert(columns=[getattr(model, key) for key in request.form], values=[["'" + request.form[key].encode('utf-8') + "'" for key in request.form]]))
	cur = conn.cursor()
	try:
		cur.execute(query[0] % query[1])
	except:
		return make_response('cant handle request', 500)
	conn.commit()
	
	conn.close()
	return make_response('OK', 200)

@app.route('/update/<table>', methods=['POST'])
def update(table = None):
	resp = make_response()
	if table == None:
		resp.status = 400
		return resp
	try:
		conn = connect()
	except:
		return make_response('Can\'t connect to database', 500)

	model = Table(table)
	column = request.form['prop']
	value = request.form['value']
	id = [request.form['id']]
	if value == 'None':
		return make_response('invalid information', 500)

	query = tuple(model.update(columns=[getattr(model, column)], \
		values=["'%s'" % value], \
		where = model.id == id))
	
	cur = conn.cursor()
	try:
		cur.execute((query[0] % query[1]).encode('utf8'))
	except: 
		return make_response('cant handle request', 500)
	conn.commit()
	
	conn.close()
	return make_response('OK', 200)

@app.route('/delete/<table>', methods=['POST'])
def delete(table = None):
	resp = make_response()
	if table == None:
		resp.status = 400
		return resp
	try:
		conn = connect()
	except:
		return make_response('Can\'t connect to database', 500)

	model = Table(table)
	id = [request.form['id']]
	
	query = tuple(model.delete(where = model.id == id))
	print query
	cur = conn.cursor()
	try:
		cur.execute(query[0] % query[1])
	except:
		return make_response('cant handle request', 500)
	conn.commit()
	
	conn.close()
	return make_response('OK', 200)

if __name__=='__main__':
	app.run(debug=True)