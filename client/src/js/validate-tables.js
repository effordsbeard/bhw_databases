var is = require('is_js');

function isInteger(value) {
	value = Number(value);
	return is.integer(value);
} 

function isDate(value) {
	let re = new RegExp(/^\d{4}-\d{2}-\d{2}$/);
	if (!re.test(value.trim()))
		return false;
	let [year, month, day] = value.split('-');
	console.log(`${month}/${day}/${year}`);
	let isdate = is.dateString(`${month}/${day}/${year}`)
	return isdate;
}

function isAlphaNumeric(value) {
	let re = new RegExp(/[a-zA-ZА-Яа-я0-9]*/);
	if (re.test(value.trim()))
		return true;
	return false;
}

var Validating = {
	passport : {
		'id' : function(value) {
			return isInteger(value);
		}, 
		'issued' : function(value) {
			return isDate(value);
		}, 
		'created' : function(value) {
			return isDate(value);
		}, 
		'type' : function(value) {
			return isAlphaNumeric(value);
		}, 
		'location' : function(value) {
			return isAlphaNumeric(value);
		}, 
		'area' : function(value) {
			return is.number(parseFloat(value));
		}, 
		'area_sum' : function(value) {
			return is.number(parseFloat(value));
		}, 
		'floors' : function(value) {
			return isInteger(value);
		}, 
		'floor_height' : function(value) {
			return is.number(parseFloat(value));
		},
		'size_description' : function(value) {
			return isAlphaNumeric(value);
		},
		'construction_date' : function(value) {
			return isDate(value);
		},
		'current_condition' : function(value) {
			return isAlphaNumeric(value);
		},
		'wall_material' : function(value) {
			return isAlphaNumeric(value);
		},
		'roof_material' : function(value) {
			return isAlphaNumeric(value);
		},
		'material_description' : function(value) {
			return isAlphaNumeric(value);
		},
		'infrastructure_description' : function(value) {
			return isAlphaNumeric(value);
		},
		'price' : function(value) {
			return is.number(parseFloat(value));
		},
	},
	owner : {
		'id': function(value) {
			return isInteger(value);
		},
		'name': function(value) {
			return isAlphaNumeric(value);
		},
		'lastname': function(value) {
			return isAlphaNumeric(value);
		},
		'patronymic': function(value) {
			return isAlphaNumeric(value);
		},
		'phone_number': function(value) {
			return isAlphaNumeric(value);
		},
		'address': function(value) {
			return isAlphaNumeric(value);
		},
	},
	contract : {
		'id': function(value) {
			return isInteger(value);;
		},
		'passport_id': function(value) {
			return isInteger(value);;
		},
		'owner_id': function(value) {
			return isInteger(value);;
		},
		'purchase_date': function(value) {
			return isDate(value);
		},
		'price': function(value) {
			return is.number(parseFloat(value));
		},
	},
};

export default Validating;
