require('../sass/index.sass');
import React from 'react';
import ReactDOM from 'react-dom';
var superagent = require('superagent');
import Table from "./components/Table";
import Emitter from "./Emitter";
import Validating from "./validate-tables";
var classnames = require('classnames');

class App extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'App';

        this.changeModel = this.changeModel.bind(this);
        this.addRequest = this.addRequest.bind(this);
        this.addData = this.addData.bind(this);
        this.validate = this.validate.bind(this);
        this.models = {
        	passport : [
        		'id', 
        		'issued', 
        		'created', 
        		'type', 
        		'location', 
        		'area', 
        		'area_sum', 
        		'floors', 
        		'floor_height',
        		'size_description',
        		'construction_date',
        		'current_condition',
        		'wall_material',
        		'roof_material',
        		'material_description',
        		'infrastructure_description',
        		'price',
        	],
        	owner : [
        		'id',
        		'name',
        		'lastname',
        		'patronymic',
        		'phone_number',
        		'address',
        	],
        	contract : [
        		'id',
        		'passport_id',
        		'owner_id',
        		'purchase_date',
        		'price',
        	],
        };

        this.pendingRequests = {};

        for (let model in this.models) {
        	this.pendingRequests[model] = {};
        }

        this.state = {
        	currentModel : 'passport'
        }

        this.passport_inputs = [];

        for (let prop of this.models.passport) {
        	this.passport_inputs.push(<div className="c c5 btn transparent"> <input className="c c16" placeholder = {prop} data-prop = {prop} data-model = 'passport' onBlur = {this.addRequest} /> </div>);
        }

        this.owner_inputs = [];

        for (let prop of this.models.owner) {
        	this.owner_inputs.push(<div className="c c5 btn transparent"> <input className="c c16" placeholder = {prop} data-prop = {prop} data-model = 'owner' onBlur = {this.addRequest} /> </div>);
        }

        this.contract_inputs = [];

        for (let prop of this.models.contract) {
        	this.contract_inputs.push(<div className="c c5 btn transparent"> <input className="c c16" placeholder = {prop} data-prop = {prop} data-model = 'contract' onBlur = {this.addRequest} /> </div>);
        }


    }

    validate(prop, value) {
        if (!this.models[this.state.currentModel].includes(prop)) {
            return;
        }
        try {
            if (value == '' || value == 'None')
                return true;
            return Validating[this.state.currentModel][prop](value);
        } catch(err) {
            console.log(this.state.model, prop, value)
            console.log(err);
        }
    }

    changeModel(model) {
    	this.setState({
    		currentModel : model,
    	});
        for (key in this.pendingRequests) {
            this.pendingRequests[key] = {};
        }
        
        Emitter.emit('model-changed', model);
    }

    addRequest(e) {
    	let value = e.target.value;
    	let prop = e.target.getAttribute('data-prop');
    	let model = e.target.getAttribute('data-model');
        if (value == '') {
            return;
        }
        let valid = this.validate(prop, value);
        if (!valid) {
            e.target.className = classnames('c', 'c16', 'wrong')
        } else {
            e.target.className = classnames('c', 'c16')
        }
    	this.pendingRequests[model][prop] = value;
    }
    
    addData() {
    	if (this.pendingRequests.length == 0)
    		return;
    	
    	for (let model in this.models) {
    		let requestData = {};

    		for (let prop in this.pendingRequests[model]) {
    			requestData[prop] = this.pendingRequests[model][prop];
    		}
            if (Object.keys(requestData).length == 0) 
                continue;
    		superagent
    			.post(`/add/${model}`)
    			.set('Content-Type', 'application/x-www-form-urlencoded')
    			.send(requestData)
    			.end(function(err, res) {
    				if (err) {
    					// alert('Element can\'t be added')
    				} else {
    					console.log('Element was added');
    				}
    			});
    	} 
    }

    render() {
        return (
            <div className="r">
            	<div className="c c12 center">
                    <div className = "r f-w f-up f-white f-b c c16">
                        Choose table
                    </div>
            		<div className = "r">
    	        		<div className = "model-type c c5 btn blue sharp d3" onClick = {() => this.changeModel('passport')}>TECH. PASSPORT</div>	
    	        		<div className = "model-type c c5 btn blue sharp d3" onClick = {() => this.changeModel('owner')}>OWNER</div>	
    	        		<div className = "model-type c c5 btn blue sharp d3" onClick = {() => this.changeModel('contract')}>CONTRACT</div>	
                    </div>
                    <div className = "r">
                    </div>

                    <div className = "r f-w f-up f-white f-b c c16">
                        Add new record
                    </div>
                    <div className = "r">
                    { this[`${ this.state.currentModel }_inputs`] }
                    </div>
                    <div className = "r">
                        <div className = "submit c c4 btn green sharp d3 center" onClick = {this.addData} >ADD</div>
                    </div>
                    <div className = "r">
                    </div>
                    <div className = "r f-w f-up f-white f-b c c16">
                        Show / Edit
                    </div>
                    <div className = "r">
                        <Table model={this.state.currentModel}/>
                    </div>

                </div>
        	</div>
        );
    }
}

export default App;

ReactDOM.render(<App />, document.getElementById('app'));