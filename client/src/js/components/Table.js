import React from 'react';
import superagent from 'superagent';
import Emitter from "../Emitter";
import Validating from "../validate-tables";
var classnames = require('classnames');

class Table extends React.Component {
    constructor(props) {
        super(props);
        this.displayName = 'Table';
        this.state = {
            rows : [],
            model : 'passport'
        }
        this.models = {
            passport : [
                'id', 
                'issued', 
                'created', 
                'type', 
                'location', 
                'area', 
                'area_sum', 
                'floors', 
                'floor_height',
                'size_description',
                'construction_date',
                'current_condition',
                'wall_material',
                'roof_material',
                'material_description',
                'infrastructure_description',
                'price',
            ],
            owner : [
                'id',
                'name',
                'lastname',
                'patronymic',
                'phone_number',
                'address',
            ],
            contract : [
                'id',
                'passport_id',
                'owner_id',
                'purchase_date',
                'price',
            ],
        };

        this.refresh = this.refresh.bind(this);
        this.update = this.update.bind(this);
        this.delete = this.delete.bind(this);
        this.refresh = this.refresh.bind(this);
        this.validate = this.validate.bind(this);
        this.refresh();
        let self = this;
        Emitter.on('model-changed', function(model) {
            self.setState({
                model: model
            }, function() {
                self.refresh();
            });
        });
    }
    componentWillReceiveProps(nextProps) {
        this.refresh();
    }

    validate(prop, value) {
        if (!this.models[this.state.model].includes(prop)) {
            return;
        }
        try {
            if (value == '' || value == 'None')
                return true;
            return Validating[this.state.model][prop](value);
        } catch(err) {
            console.log(this.state.model, prop, value)
            console.log(err);
        }
    }

    update(e) {
        let self = this;
        let data_id = e.target.getAttribute('data-id');
        let data_prop = e.target.getAttribute('data-prop');
        let model = e.target.getAttribute('data-model');
        let value = e.target.value;
        if (value == '') {
            return;
        }
        let valid = this.validate(data_prop, value);
        if (!valid) {
            e.target.className = classnames('c', 'c16', 'wrong')
            return;
        } else {
            e.target.className = classnames('c', 'c16')
        }

        superagent
            .post(`/update/${model}`)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({
                "prop" : data_prop,
                "id" : data_id,
                "value" : value
            })
            .end(function(err, res) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(`${value} with ${data_id} in ${model} updated`);
                }
            });
    }
    delete(e) {
        let data_id = e.target.getAttribute('data-id');
        let model = e.target.getAttribute('data-model');
        let self = this;
        superagent
            .post(`/delete/${model}`)
            .set('Content-Type', 'application/x-www-form-urlencoded')
            .send({
                "id" : data_id
            })
            .end(function(err, res) {
                if (err) {
                    console.log('Element can\'t be deleted');
                } else {
                    console.log(`Element with ${data_id} in ${model} deleted`);
                    self.refresh();
                }
            });
    }
    refresh() {
        let self = this;
        superagent
            .get(`/get/${self.state.model}`)
            .set('Content-Type', 'application/json')
            .end(function(err, res) {
                if (res.status !== 200) {
                    alert('Can\'t connect to database')
                } else {
                    let rows = JSON.parse(res.text).objects;
                    let viewRows = [];
                    for (let row of rows) {
                        let arr = row[Object.keys(row)[0]];
                        let id = arr['0'][1];
                        let fields = [];
                        for (let field of arr) {
                            fields.push(
                                <div className="c c3">
                                    
                                    <div className="r">
                                        <div className="c c16 f-white f-b">
                                            {field[0]}
                                        </div>
                                        <input className={classnames('c', 'c16', {'wrong' :  !self.validate(field[0], field[1]) } )} data-model={self.state.model} data-id={id} placeholder={field[1] != 'None' && field[1] != '' ? field[1] : field[0]} data-prop={field[0]} onBlur={self.update} />
                                    </div>
                                </div>
                            );
                        }
                        let viewRow = 
                            <div className="r">
                                <div className="r">
                                    <div className="c c1 btn transparent f-white">{id}</div>
                                    <div className="c c1 btn red f-b" data-model={self.state.model} data-id={id} onClick={self.delete}>X</div>
                                </div>
                                {fields}
                                
                            </div>;
                        viewRows.push(viewRow);
                    }
                    self.setState({
                        'rows' : viewRows
                    });
                }
            });
    }
    render() {
        return (
            <div>
                <div className="c c2 btn blue f-b" onClick={this.refresh}>REFRESH</div> 
                <div className="r"></div>
                <div>
                    {this.state.rows}
                </div>
            </div>
        );
    }
}

export default Table;