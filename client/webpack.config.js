const webpack = require('webpack')

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
	entry: './src/js/app.js',
	output: {
		path: './dist/js',
		filename: 'app.bundle.js',
	},
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				exclude: /node_modules/,
				loader: 'babel',
			},
			{ 
				test: /\.sass$/, 
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract(["css", "sass"])
			},
		],
	},
	plugins: [
		// new webpack.optimize.UglifyJsPlugin({
		// 	compress: {
		// 		warnings: false,
		// 	},
		// 	output: {
		// 		comments: false
		// 	}
		// }),
		new ExtractTextPlugin("/dist/css/bundle.css"),
		new webpack.optimize.DedupePlugin(),
	],
}