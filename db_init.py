# -*- coding: utf8 -*-
import psycopg2
conn = psycopg2.connect('dbname=%s user=%s host=%s password=%s' % \
	('tech_passports', \
	'pguser', \
	'localhost', \
	'pg_SecurE' ))
cur = conn.cursor()

cur.execute("""
create table passport (id int primary key not null, 
	issued date,
	created date,
	type varchar(20),
	location varchar(200),
	area int,
	area_sum int,
	floors int,
	floor_height int,
	size_description text,
	construction_date date,
	current_condition text, wall_material varchar(50),
	roof_material varchar(50),
	material_description text,
	infrastructure_description text,
	price float4
);
""")

cur.execute("""
create table owner ( id int primary key not null, 
	name varchar(30), 
	lastname varchar(30), 
	patronymic varchar(30), 
	phone_number varchar(12), 
	address varchar(200)
);
""")

cur.execute("""
create table contract(id int primary key not null,  
	passport_id int, 
	owner_id int, 
	purchase_date date, 
	price float8 
);
""")

cur.execute("""
insert into passport values (
'1',
'1999-10-26',
'1998-09-16',
'Дом',
'ул. Домогаевская, 12',
'98',
'167',
'2',
'3',
'',
'1998-09-16',
'',
'',
'',
'',
'',
'12345'
);
""")

cur.execute("""
insert into passport values (
'2',
'1997-1-06',
'2003-11-16',
'Дом',
'ул. Коммунистическая, 32',
'45',
'178',
'3',
'4',
'',
'2003-11-16',
'',
'',
'',
'',
'',
'47859'
);
""")

cur.execute("""
insert into passport values (
'3',
'2001-10-16',
'2007-12-10',
'Дом',
'ул. Трактористов, 74',
'54',
'120',
'2',
'1',
'',
'2007-12-10',
'',
'',
'',
'',
'',
'21789'
);
""")

cur.execute("""
insert into passport values (
'4',
'2002-10-21',
'2007-12-23',
'Дом',
'ул. Южная, 57',
'65',
'214',
'3',
'2',
'',
'2007-12-23',
'',
'',
'',
'',
'',
'35789'
);
""")

cur.execute("""
insert into passport values (
'5',
'2008-11-11',
'2015-09-01',
'Дом',
'ул. Южная, 57',
'78',
'112',
'2',
'3',
'',
'2015-09-01',
'',
'',
'',
'',
'',
'96512'
);
""")

cur.execute("""
insert into owner values (
'1',
'Ксения',
'Павлова',
'Валерьевна',
'89161234567',
'ул. Воинов-Интернационалистов, 45'
);
""")

cur.execute("""
insert into owner values (
'2',
'Анна',
'Иванова',
'Петровна',
'89268745411',
'ул. Петрозаводская, 4'
);
""")

cur.execute("""
insert into owner values (
'3',
'Алексей',
'Заварухин',
'Валентинович',
'89295547781',
'ул. Карла Маркса, 47'
);
""")

cur.execute("""
insert into owner values (
'4',
'Александр',
'Шахов',
'Егорович',
'89196654477',
'ул. Первомайская, 17'
);
""")

cur.execute("""
insert into owner values (
'5',
'Мария',
'Юровская',
'Владигоровна',
'89176664411',
'ул. Юности, 9'
);
""")

cur.execute("""
insert into contract values (
'1',
'1',
'1',
'2008-09-16',
'123456'
);
""")

cur.execute("""
insert into contract values (
'2',
'1',
'5',
'2012-04-06',
'547896'
);
""")

cur.execute("""
insert into contract values (
'3',
'3',
'2',
'1999-02-18',
'369874'
);
""")

cur.execute("""
insert into contract values (
'4',
'5',
'2',
'2007-09-26',
'541287'
);
""")

cur.execute("""
insert into contract values (
'5',
'1',
'2',
'2015-08-11',
'347852'
);
""")

conn.commit();
conn.close();